<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package Core
 * @link    https://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace Contao;


/**
 * Class ContentSliderStart
 *
 * @package Dse\ElementsBundle\Elements
 */
class ContentSliderElementStart extends \ContentElement
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'ce_sliderel_start';


	/**
	 * Generate the content element
	 */
	protected function compile()
	{
		if (TL_MODE == 'BE')
		{
			$this->strTemplate = 'be_wildcard';
			$this->Template = new \BackendTemplate($this->strTemplate);			
		}
		else
		{
			$this->strTemplate = $this->sliderel_start_tpl;
			$this->Template = new \FrontendTemplate($this->strTemplate);                        
			$this->Template->setData($this->arrData);
		}
	}
}
