<?php

namespace Dse\ElementsBundle\ElementSliderwrapper\ContaoManager;

use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Dse\ElementsBundle\ElementSliderwrapper;
use Contao\CoreBundle\ContaoCoreBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(ElementSliderwrapper\DseElementSliderwrapper::class)
                ->setLoadAfter([ContaoCoreBundle::class])
        ];
    }
}
