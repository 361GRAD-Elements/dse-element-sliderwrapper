<?php

/**
 * 361GRAD Element Separator
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['sliderElementStart'][0] = 'Schieber Element Wrapper Start';
$GLOBALS['TL_LANG']['CTE']['sliderElementStart'][1] = 'Erstellen Sie ein Schieberelement-Element.';
$GLOBALS['TL_LANG']['CTE']['sliderElementStop'][0] = 'Schieber Element Wrapper Stop';
$GLOBALS['TL_LANG']['CTE']['sliderElementStop'][1] = 'Erstellen Sie ein Schieberelement-Wrapper-Stoppelement.';

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Randeinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Rand oben', 'Hier können Sie Margin zum oberen Rand des Elements hinzufügen (nur nummern)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Rand unten', 'Hier können Sie dem unteren Rand des Elements Rand hinzufügen (nur nummern)'];