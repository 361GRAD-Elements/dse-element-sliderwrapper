<?php

/**
 * 361GRAD Element Sliderwrapper
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_CTE']['slider']['sliderStart'] = 'ContentSliderStartExt';
$GLOBALS['TL_CTE']['slider']['sliderStop'] = 'ContentSliderStopExt';
$GLOBALS['TL_CTE']['slider']['sliderElementStart'] = 'ContentSliderElementStart';
$GLOBALS['TL_CTE']['slider']['sliderElementStop'] = 'ContentSliderElementStop';

/**
 * Wrapper elements
 */
$GLOBALS['TL_WRAPPERS']['start'][] = 'sliderElementStart';
$GLOBALS['TL_WRAPPERS']['stop'][] = 'sliderElementStop';
