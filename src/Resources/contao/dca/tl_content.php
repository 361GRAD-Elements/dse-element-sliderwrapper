<?php

/**
 * 361GRAD Element Separator
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['sliderStart'] = '{type_legend},type,headline;{slider_legend},sliderStartSlide;{bcsettings_legend},bcslidertype;{template_legend:hide},slider_start_tpl;{margin_legend},dse_marginTop,dse_marginBottom;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space;{invisible_legend:hide},invisible,start,stop';
$GLOBALS['TL_DCA']['tl_content']['palettes']['sliderStop'] = '{type_legend},type,headline;{template_legend:hide},slider_stop_tpl;{protected_legend:hide},protected;{expert_legend:hide},guests;{invisible_legend:hide},invisible,start,stop';
$GLOBALS['TL_DCA']['tl_content']['palettes']['sliderElementStart'] = '{type_legend},type;{template_legend:hide},sliderel_start_tpl;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space;{invisible_legend:hide},invisible,start,stop';
$GLOBALS['TL_DCA']['tl_content']['palettes']['sliderElementStop'] = '{type_legend},type;{template_legend:hide},sliderel_stop_tpl;{protected_legend:hide},protected;{expert_legend:hide},guests;{invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['slider_start_tpl'] = array
    (
    'label' => &$GLOBALS['TL_LANG']['tl_content']['slider_start_tpl'],
    'default' => 'ce_slider_start',
    'exclude' => true,
    'inputType' => 'select',
    'options_callback' => array('tl_content_extras', 'getSliderTemplates'),
    'eval' => array('tl_class' => 'w50'),
    'sql' => "varchar(32) NOT NULL default 'ce_slider_start'"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['slider_stop_tpl'] = array
    (
    'label' => &$GLOBALS['TL_LANG']['tl_content']['slider_stop_tpl'],
    'default' => 'ce_slider_stop',
    'exclude' => true,
    'inputType' => 'select',
    'options_callback' => array('tl_content_extras', 'getSliderTemplates'),
    'eval' => array('tl_class' => 'w50'),
    'sql' => "varchar(32) NOT NULL default 'ce_slider_stop'"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['sliderel_start_tpl'] = array
    (
    'label' => &$GLOBALS['TL_LANG']['tl_content']['sliderel_start_tpl'],
    'default' => 'ce_sliderel_start',
    'exclude' => true,
    'inputType' => 'select',
    'options_callback' => array('tl_content_extras', 'getSliderElementTemplates'),
    'eval' => array('tl_class' => 'w50'),
    'sql' => "varchar(32) NOT NULL default 'ce_slider_start'"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['sliderel_stop_tpl'] = array
    (
    'label' => &$GLOBALS['TL_LANG']['tl_content']['sliderel_stop_tpl'],
    'default' => 'ce_sliderel_stop',
    'exclude' => true,
    'inputType' => 'select',
    'options_callback' => array('tl_content_extras', 'getSliderElementTemplates'),
    'eval' => array('tl_class' => 'w50'),
    'sql' => "varchar(32) NOT NULL default 'ce_slider_stop'"
);

/**
 * Class tl_content_extras
 *
 * Provide miscellaneous methods that are used by the data configuration array.
 * @package   DSE Extras
 * @author    Yuriy Davats 
 * @license   GNU 
 * @copyright 361GRAD
 */
class tl_content_extras extends Backend {

    /**
     * Import the back end user object
     */
    public function __construct() {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }


    /**
     * Return all slider templates as array
     * @return array
     */

    public function getSliderTemplates() {

        return $this->getTemplateGroup('ce_slider_');

    }



    /**
     * Return all slider element templates as array
     * @return array
     */

 

    public function getSliderElementTemplates() {

        return $this->getTemplateGroup('ce_sliderel_');

    }

}